document.getElementById("hello_text").textContent = "はじめてのJavaScript";


var count = 0;
var cells;
//ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },  //
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t:{
        class:"t",
        pattern:[
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s:{
        class: "s",
        pattern:[
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z:{
        class:"z",
        pattern:[
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j:{
        class:"j",
        pattern:[
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l:{
        class:"l",
        pattern:[
            [0, 0, 1],
            [1, 1, 1]
        ]
    }

}

loadTable();
setInterval(function(){
    count++;//何回目かを数えるために変数countを1ずつ増やす
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; //何回目かを文字にまとめて表示する
    if(hasFallingBlock()){ //落下中のブロックがあるか確認する
        fallBlocks(); //あればブロックを落とす
    } else{           //なければ
        deleteRow();//そろっている行を消す
        generateBlock();//ランダムにブロックを作成する
    }
},500);


//////////キーイベント///////////////
document.addEventListener("keydown",onKeyDown); 
function onKeyDown(event){
    if(event.keyCode === 37){
        moveLeft();
    } else if(event.keyCode === 39){
        moveRight();
    }
}
////////////////////////////////////


////////ここから関数の宣言////////

function loadTable(){
    cells = [];
    var td_array = document.getElementsByTagName("td"); //200個の要素を持つ配列
    var index = 0;
    for(var row = 0;row < 20; row++){
        cells[row] = []; //配列のそれぞれの要素を配列にする（二次元配列にする）
        for(var col = 0; col < 10; col++){
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks(){
    // 1.そこについていないか？
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {  //一番下にブロックがあるのでフラグがfalse
          isFalling = false;
          return; // 一番下の行にブロックがいるので落とさない
        }
    }
    //下から二番目の行からら繰り返しクラスを下げていく
    // 2.1マス下に別のブロックがないか
    for(var row = 18; row >= 0; row--){
        for(var col = 0; col < 10; col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
                    isFalling = false;
                    return;//一つ下のマスにブロックがいるので落とさない
                }
            }
        }
    }
      // 下から二番目の行から繰り返しクラスを下げていく
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
           if (cells[row][col].blockNum === fallingBlockNum) {
              cells[row + 1][col].className = cells[row][col].className;
            cells[row + 1][col].blockNum = cells[row][col].blockNum;
            cells[row][col].className = "";
            cells[row][col].blockNum = null;
            }
　      }
    }
}


var isFalling = false;
function hasFallingBlock(){
    //落下中のブロックがあるか確認する
    return isFalling;
}

function deleteRow(){
    //そろっている行を消す
}
var fallingBlockNum = 0;
function generateBlock(){
    //ランダムにブロックを作成する
    // 1.ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks); //名前の配列を取得
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];   //名前の一覧からランダムに選ぶ
    var nextBlock = blocks[nextBlockKey];   //選んだ名前から要素を出億
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 2.選んだパターンを基にブロックを配置する
    var pattern = nextBlock.pattern;
    for(var row = 0; row < pattern.length; row++){
        for(var col = 0; col < pattern[row].length; col++){
            if(pattern[row][col]){
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3.落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}
function moveRight(){
    //ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        if (cells[row][9].blockNum === fallingBlockNum) {  //一番右にブロックがあるので
          return; // 一番右の列にブロックがあるのでそれ以上右には動かせない
        }
    }
    
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col > 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
function moveLeft(){
    //ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        if (cells[row][0].blockNum === fallingBlockNum) {  //一番右左ブロックがあるので
          return; // 一番左の列にブロックがあるのでそれ以上右には動かせない
        }
    }

    for (var row = 0; row < 20;  row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
            cells[row][col - 1].className = cells[row][col].className;
            cells[row][col - 1].blockNum = cells[row][col].blockNum;
            cells[row][col].className = "";
            cells[row][col].blockNum = null;
            }
        }
    }
}
